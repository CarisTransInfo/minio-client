const Minio = require('minio');
const getFileModule = require('./internal/getFile');
const getFileStreamModule = require('./internal/getFileStream');
const saveFileModule = require('./internal/saveFile');

module.exports = ({
  endPoint,
  port = '443',
  accessKey,
  secretKey,
  bucket,
  useSSL = true,
  nullOnMissing = false
}) => {

  const minio = new Minio.Client({
    endPoint,
    port: parseInt(port),
    accessKey,
    secretKey,
    useSSL: useSSL === 'true' || useSSL === true
  });

  const getFile = getFileModule(minio, bucket, nullOnMissing === 'true' || nullOnMissing === true);
  const getFileStream = getFileStreamModule(minio, bucket);
  const saveFile = saveFileModule(minio, bucket);

  const getJson = (file) => getFile(file, 'utf8').then(text => !text ? null : JSON.parse(text));

  return {
    getFile,
    getFileStream,
    saveFile,
    report: {
      getPdf: (caseId) =>
        getFile(`${caseId}/report/final.pdf`),
      getPdfStream: (caseId) =>
        getFileStream(`${caseId}/report/final.pdf`),
      savePdf: (caseId, contents) =>
        saveFile(`${caseId}/report/final.pdf`, contents),
      getXml: (caseId) =>
        getFile(`${caseId}/report/final.xml`, 'utf8'),
      getXmlStream: (caseId) =>
        getFileStream(`${caseId}/report/final.xml`),
      saveXml: (caseId, contents) =>
        saveFile(`${caseId}/report/final.xml`, contents),
      getBrief: (caseId) =>
        getJson(`${caseId}/report/brief.json`),
      saveBrief: (caseId, contents) =>
        saveFile(`${caseId}/report/brief.json`, JSON.stringify(contents, null, 2)),
      getLabStatus: (caseId) =>
        getJson(`${caseId}/report/status.json`),
      saveLabStatus: (caseId, contents) =>
        saveFile(`${caseId}/report/status.json`, JSON.stringify(contents, null, 2)),
      getIhc: (caseId) =>
        getJson(`${caseId}/report/ihc.json`),
      saveIhc: (caseId, contents) =>
        saveFile(`${caseId}/report/ihc.json`, JSON.stringify(contents, null, 2)),
      getDrugs: (caseId) =>
        getJson(`${caseId}/report/drugs.json`),
      saveDrugs: (caseId, contents) =>
        saveFile(`${caseId}/report/drugs.json`, JSON.stringify(contents, null, 2))
    }
  }
}
