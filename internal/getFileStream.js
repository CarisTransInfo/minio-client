const debug = require('debug')('minio-client:internal/getFile');
module.exports = (cli, bucket) => (path) => new Promise((resolve, reject) => {
  cli.getObject(
    bucket,
    path,
    (err, stream) => {
      if(err)
        reject(err);
      else
        resolve(stream);
    });
});
