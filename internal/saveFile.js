module.exports = (cli, bucket) => (file, contents) => new Promise((resolve, reject) => {

  // Check file extension
  let mime;
  switch(file.split('.').pop()) {
    case 'pdf':
      mime = 'application/pdf';
      break;
    case 'xml':
      mime = 'application/xml';
      break;
    case 'json':
      mime = 'application/json';
      break;
    default:
      mime = 'application/octet-stream';
  }

  cli.putObject(
    bucket,
    file,
    contents,
    {
      createdOn: new Date(),
      createdBy: 'ETL',
      version: 1,
      'Content-Type': mime
    },
    (err, etag) => {
      if(!err)
        resolve(etag);
      else
        reject(err);
    });
});
