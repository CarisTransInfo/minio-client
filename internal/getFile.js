const debug = require('debug')('minio-client:internal/getFile');
const getFileStreamModule = require('./getFileStream');
module.exports = (cli, bucket, nullOnMissing) => async (path, encoding) => {

  const stream = await getFileStreamModule(cli, bucket)(path).catch(() => null);

  if(!stream)
    return Promise.resolve(null);

  return new Promise((resolve) => {
    let data = [];
    stream.on('data', (chunk) => {
      data.push(chunk);
    });
    stream.on('end', () => {
      resolve(encoding ? Buffer.concat(data).toString('utf8') : Buffer.concat(data));
    });
  });
};
