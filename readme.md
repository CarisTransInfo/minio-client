# Minio Client

[![NPM Version](https://img.shields.io/npm/v/@carisls/minio-client.svg?style=flat)](https://www.npmjs.org/package/@carisls/minio-client)
[![NPM Downloads](https://img.shields.io/npm/dm/@carisls/minio-client.svg?style=flat)](https://npmcharts.com/compare/@carisls/minio-client?minimal=true)
[![Install Size](https://packagephobia.now.sh/badge?p=%40carisls%2Fminio-client)](https://packagephobia.now.sh/result?p=%40carisls%2Fminio-client)
[![CircleCI](https://circleci.com/bb/CarisTransInfo/minio-client.svg?style=shield&circle-token=e556aeb465a9e399cc728f20cf3e311942cb14f5)](https://circleci.com/bb/CarisTransInfo/minio-client)

The purpose of this package is to facilitate Minio/S3 integration for Caris
projects.

## Instructions
Those are currently available methods:

- report
  - getPdf
  - savePdf
  - getXml
  - saveXml
  - getBrief
  - saveBrief
  - getDrugs
  - saveDrugs
  - getIhc
  - saveIhc

(documentation in development)
